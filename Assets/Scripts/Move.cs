﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float forceValue;
    public float jumpValue;
    private Rigidbody rb;
    private AudioSource audiosource;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audiosource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && Math.Abs(rb.velocity.y) < 0.01f)
        {
            rb.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
            audiosource.Play();
        }
        
        if (Input.touchCount == 1)
        {
            if (Input.touches[0].phase == TouchPhase.Began && Math.Abs(rb.velocity.y) < 0.01f)
            {
                rb.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
                audiosource.Play();
            }
        }

    }
    
    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical")) * forceValue);
        
        rb.AddForce(new Vector3(Input.acceleration.x,
            0,
            Input.acceleration.y) * forceValue);
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Coin")
        {
            print("One coin collected");
            Destroy(collider.gameObject);
        }
    }
    
}